<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Events;

class EventController extends Controller
{
    function getEvent(){
       
        /* Untuk Authenticate
        if (! $user = JWTAuth::parseToken()->authenticate()) {
            return response()->json(['user_not_found'], 404);
        } */

        $eventList = Events::get();
            return response()->json($eventList, 200);
    }
    
    function addEvent(Request $request){

        DB::beginTransaction();
        try{

            

            $event_name = $request->input('event_name');
            $event_date = $request->input('event_date');
            $price = $request->input('price');
            $ticket_quota = $request->input('ticket_quota');

            $event = new Events;

            $event->event_name = $event_name;
            $event->event_date = $event_date;
            $event->price = $price;
            $event->ticket_quota = $ticket_quota;
            $event->save();

            $eventList = Events::get();

    

            DB::commit(); //commit untuk masuk kedalam database bila berhasil
            return response()->json($eventList, 200);

        }
        catch(\Exception $e){
            DB::rollBack();
            return response()->json(["message" => $e->getMessage()], 500);
            
        }

    }
}
