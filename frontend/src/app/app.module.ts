import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { ApiService } from './api.service';

import { AppComponent } from './app.component';
import { EventListComponent } from './components/event-list/event-list.component';
import { EventMasterComponent } from './components/event-master/event-master.component';

@NgModule({
  declarations: [
    AppComponent,
    EventListComponent,
    EventMasterComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
  ],
  providers: [
    ApiService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
