import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import 'rxjs/add/operator/map';

@Injectable()
export class ApiService {

  constructor( private http:Http ) { 
  }

  getEventList(){
    
    return this.http  .get('http://localhost:8000/api/event')
                      .map(result => result.json());
  }

  addEvent(event_name:string, event_date:string, price:number, ticket_quota:number){
      let data = {
        "event_name" : name,
        "event_date" : event_date,
        "price": price,
        "ticket_quota": ticket_quota,
      };
      let body = JSON.stringify(data);
      let headers = new Headers({ 
        "Content-Type" : "application/json"
      });
      let options = new RequestOptions({ headers : headers });

      return this.http .post('http://localhost:8000/api/event/add', body, options)
                .map(result => result.json());
  }


}
