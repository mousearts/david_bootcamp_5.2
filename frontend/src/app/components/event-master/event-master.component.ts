import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../api.service';

@Component({
  selector: 'app-event-master',
  templateUrl: './event-master.component.html',
  styleUrls: ['./event-master.component.css']
})
export class EventMasterComponent implements OnInit {

  constructor(private api:ApiService) { }

  event_name: string ="";
  event_date: string ="";
  price: number = 0;
  ticket_quota: number =0;

  eventList:Object[];

  ngOnInit() {
  }

  addEvent(){
    this.api  .addEvent(this.event_name, this.event_date, this.price, this.ticket_quota)
              .subscribe(result => this.eventList = result);

    this.event_name="";
    this.event_date="";
    this.price= 0;
    this.ticket_quota= 0;
  }

}
